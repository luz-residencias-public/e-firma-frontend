import { OnInit } from '@angular/core';
import { Component } from '@angular/core';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];

    ngOnInit() {
        this.model = [
            {
                label: 'Inicio',
                icon: 'pi pi-home',
                routerLink: ['/admin']
            },
            {
                label: 'Documentos',
                icon: 'pi pi-file',
                routerLink: ['/admin/mis-documentos']
            },
            {
                label: 'Usuario',
                icon: 'pi pi-user',
                routerLink: ['/admin/usuarios']
            },
            {
                label: 'Configuración',
                icon: 'pi pi-cog',
                routerLink: ['/admin/configuracion']
            }
        ];
    }
}
