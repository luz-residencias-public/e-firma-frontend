import { Component, OnInit } from '@angular/core';
import { Documento } from 'src/app/firma-electronica/models/documento';
import { Firma } from 'src/app/firma-electronica/models/firma';
import { DocumentosService } from 'src/app/firma-electronica/service/documentos/documentos.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.scss'],
  providers: [MessageService, ConfirmationService]
})

export class DocumentosComponent implements OnInit {


  documento: Documento = {};
  documentos: Documento[] = [];

  firma: Firma = {};

  selectedDocumentos: Documento[] = [];
  documentoDialog: boolean = false;
  signDialog: boolean = false;
  submitted: boolean = false;
  cols: any[] = [];
  statuses: any[] = [];
  rowsPerPageOptions = [5, 10, 20];

  deleteDocumentosDialog: boolean = false;
  deleteDocumentoDialog: boolean = false;

  constructor(private documentosService: DocumentosService, private messageService: MessageService, private confirmationService: ConfirmationService) { }


  ngOnInit() {
    this.documentosService.getDocumentos().then(
      data => this.documentos = data
    );

    this.cols = [
      { field: 'id', header: 'id' },
      { field: 'tipoDocumento', header: 'tipoDocumento' },
      { field: 'nombreDocumento', header: 'nombreDocumento' },
      { field: 'descripcion', header: 'descripcion' },
      { field: 'tipoArchivo', header: 'tipoArchivo' }
    ];

  }

  editDocumento(documento: Documento) {
    //this.usuario = { ...usuario };
    this.documentoDialog = true;
  }

  firmarDocumento(firma: Firma) {
    this.firma.idDocumento = this.documento.id;
    this.firma.documentoBase64 = this.documento.documentoBase64;
    console.log(this.firma);

    this.documentosService.firmar(this.firma)
        .then(data => {
          console.log(data);
          this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Documento Firmado', life: 3000 });
          this.signDialog = false;
          this.documento.rutaDocumento = data.urlDescarga;
          this.documento.documentoFirmadoBase64 = data.documentoFirmadoBase64;
        })
        .catch(error => {
          console.error(error); // Maneja el error
        });

  }

  deleteDocumento(documento: Documento) {
    var idDocumento = documento.id;

    if (idDocumento != undefined) {
      this.documentosService.deleteDocumento(idDocumento)
        .then(mensaje => {
          this.deleteDocumentoDialog = true;
          this.documento = { ...documento };
        })
        .catch(error => {
          console.error(error); // Maneja el error
        });
    }
  }

  openNew() {
    this.documento = {};
    this.submitted = false;
    this.documentoDialog = true;
  }

  openSignDocumento(documento:Documento) {
    this.documento = documento;
    this.signDialog = true;
  }

  hideDialog() {
    this.documentoDialog = false;
    this.submitted = false;
  }

  createUsuario(documento: Documento) {
    this.documentosService.createDocumento(documento)
      .then(mensaje => {
        this.confirmUserCreated();
      })
      .catch(error => {
        console.error(error); // Maneja el error
      });
  }
  confirmUserCreated() {
    this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Documento Creado', life: 3000 });
  }

  onFileSelect(event: { files: string | any[]; }, tipo:string) {
    if (event.files && event.files.length > 0) {
      const file = event.files[0];
      const reader = new FileReader();

      reader.onload = (e: any) => {
        let base64String: string = e.target.result;
        const base64WithoutPrefix = base64String.split(',')[1];
        if(tipo==='pdf'){
            this.documento.documentoBase64 = base64WithoutPrefix;
        }else if(tipo==='cer'){ 
            this.firma.cerBase64 = base64WithoutPrefix;
        }else if(tipo==='key'){
            this.firma.keyBase64 = base64WithoutPrefix;
        }else if(tipo==='pfx'){
          this.firma.certificadoBase64 = base64WithoutPrefix;
      }

        
      };

      reader.readAsDataURL(file);
    }
  }

  createDocumento(documento: Documento) {
    if (documento.documentoBase64) {
      console.log("Si tiene documentos");

    }

    this.documentosService.createDocumento(documento)
      .then(data => {
        this.confirmUserCreated();
        console.log('nuevo documento', data)
        this.documentos.push(data);
      })
      .catch(error => {
        console.error(error); // Maneja el error
      });
  }

  confirmDelete() {
    this.deleteDocumentoDialog = false;
    this.documentos = this.documentos.filter(val => val.id !== this.documento.id);
    this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Documento Eliminado', life: 3000 });
    this.documento = {};
  }

  confirmDeleteSelected() {
    this.deleteDocumentosDialog = false;
    this.documentos = this.documentos.filter(val => !this.selectedDocumentos.includes(val));
    this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Documento Eliminado', life: 3000 });
    this.selectedDocumentos = [];
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

}
