import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfiguracionComponent } from './configuracion.component';
import { ConfiguracionRoutingModule } from './configuracion-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ConfiguracionRoutingModule

    ],
    declarations: [
        ConfiguracionComponent
    ]
})
export class ConfiguracionModule { }
