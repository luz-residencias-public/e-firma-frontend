import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/firma-electronica/models/usuario';
import { UsuariosService } from 'src/app/firma-electronica/service/usuarios/usuarios.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class UsuariosComponent implements OnInit{
  productDialog: boolean = false;

  deleteProductDialog: boolean = false;

  deleteProductsDialog: boolean = false;

  usuarios: Usuario[] = [];

  usuario: Usuario = {};

  selectedProducts: Usuario[] = [];

  submitted: boolean = false;

  cols: any[] = [];

  statuses: any[] = [];

  rowsPerPageOptions = [5, 10, 20];

  constructor(private usuariosService: UsuariosService, private messageService: MessageService, private confirmationService: ConfirmationService) { }

  ngOnInit() {

        this.usuariosService.getUsuarios().then(
            data => this.usuarios = data
        );

      this.cols = [
        { field: 'id', header: 'id' },  
        { field: 'expediente', header: 'expediente' },
        { field: 'nombre', header: 'nombre' },
        { field: 'correo', header: 'correo' },
        { field: 'rol', header: 'rol' }
          
      ];
  }

  openNew() {
      this.usuario = {};
      this.submitted = false;
      this.productDialog = true;
  }

  updateUsuario(usuario: Usuario) {

    this.usuario = { ...usuario };
      this.productDialog = true;
    var idUsuario = usuario.id;

    if(idUsuario != undefined){
        this.usuariosService.editUsuario(usuario, idUsuario)
        .then(mensaje => {
            this.confirmUserdited();
        })
        .catch(error => {
            console.error(error); // Maneja el error
        });
        }

      
  }


  editUsuario(usuario: Usuario) {
      //this.usuario = { ...usuario };
      this.productDialog = true;
  }

  deleteUsuario(usuario:Usuario) {
    var idUsuario = usuario.id;

    if(idUsuario != undefined){
        this.usuariosService.deleteUsuario(idUsuario)
        .then(mensaje => {
            this.deleteProductDialog = true;
            this.usuario = { ...usuario };
        })
        .catch(error => {
            console.error(error); // Maneja el error
        });
        }
  }


  deleteProduct(usuario: Usuario) {
      this.deleteProductDialog = true;
      this.usuario = { ...usuario };
  }

  confirmDeleteSelected() {
      this.deleteProductsDialog = false;
      this.usuarios = this.usuarios.filter(val => !this.selectedProducts.includes(val));
      this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Usuario Eliminado', life: 3000 });
      this.selectedProducts = [];
  }

  confirmDelete() {
      this.deleteProductDialog = false;
      this.usuarios = this.usuarios.filter(val => val.id !== this.usuario.id);
      this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Usuario Eliminado', life: 3000 });
      this.usuario = {};
  }

  confirmUserCreated() {
    this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Usuario Creado', life: 3000 });
}

confirmUserdited() {
    this.messageService.add({ severity: 'success', summary: 'Exitoso', detail: 'Usuario editado', life: 3000 });
}

  hideDialog() {
      this.productDialog = false;
      this.submitted = false;
  }

  createUsuario(usuario:Usuario){
    this.usuariosService.createUsuario(usuario)
        .then(mensaje => {
            this.confirmUserCreated();
        })
        .catch(error => {
            console.error(error); // Maneja el error
        });
  }


  onGlobalFilter(table: Table, event: Event) {
      table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }
}
