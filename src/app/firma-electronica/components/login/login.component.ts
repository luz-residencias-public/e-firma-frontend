import { Component } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { LoginService } from '../../service/login/login.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
	templateUrl: './login.component.html'
})
export class LoginComponent {

	rememberMe: boolean = false;

	constructor(private layoutService: LayoutService, private loginService: LoginService, private router: Router) {}
	responseMessage: string = '';

	onLogin(form: NgForm) {
		if (form.valid) {
		  this.loginService.login(form.value).subscribe(
			(response) => {
			  console.log('Login exitoso', response);
			  console.log(response.status)
			  if(response.status!='Fail'){
				this.router.navigate(['/admin']);
			  }else{
				this.responseMessage = response.message;
			  }
			},
			(error) => {
			  console.error('Error en el login', error);
			  this.responseMessage = 'Error en el login: ' + error.message;
			}
		  );
		}
	  }

	get dark(): boolean {
		return this.layoutService.config.colorScheme !== 'light';
	}

}
