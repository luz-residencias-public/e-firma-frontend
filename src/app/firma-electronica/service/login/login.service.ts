import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private loginUrl = 'http://localhost:8083/api/autenticacion/login'; // URL a la API


  constructor(private http: HttpClient) { }

  login(userData: { email: string; password: string }): Observable<any> {
    return this.http.post<any>(this.loginUrl, userData);
  }
  
}
