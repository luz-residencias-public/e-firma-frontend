import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Usuario } from 'src/app/firma-electronica/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient) { }

  getUsuarios() { 
    return this.http.get<any>('http://localhost:8083/api/usuarios/')
      .toPromise()
      .then(res => res.data as Usuario[])
  }

  deleteUsuario(idUsuario:number) { 
    return this.http.delete<any>('http://localhost:8083/api/usuarios/'+idUsuario)
      .toPromise()
      .then(res => res.data.message)
  }

  createUsuario(usuario:Usuario) { 
    return this.http.post<any>('http://localhost:8083/api/usuarios/', usuario)
      .toPromise()
      .then(res => res.data.data)
  }

  editUsuario(usuario:Usuario, idUsuario:number) { 
    return this.http.put<any>('http://localhost:8083/api/usuarios/'+idUsuario, usuario)
      .toPromise()
      .then(res => res.data.data)
  }

}
