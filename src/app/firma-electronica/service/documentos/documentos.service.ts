import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Documento } from 'src/app/firma-electronica/models/documento';
import { Firma } from '../../models/firma';


@Injectable({
  providedIn: 'root'
})
export class DocumentosService {

  constructor(private http: HttpClient) { }

  getDocumentos() {
    return this.http.get<any>('http://localhost:8083/api/documentos/')
      .toPromise()
      .then(res => res.data as Documento[])
  }

  deleteDocumento(idDocumento:number) { 
    return this.http.delete<any>('http://localhost:8083/api/documentos/'+idDocumento)
      .toPromise()
      .then(res => res.data.message)
  }

  createDocumento(documento:Documento) { 
    return this.http.post<any>('http://localhost:8083/api/documentos/', documento)
      .toPromise()
      .then(res => res.data)
  }

  editUsuario(documento:Documento, idDocumento:number) { 
    return this.http.put<any>('http://localhost:8083/api/documentos/'+idDocumento, documento)
      .toPromise()
      .then(res => res.data.data)
  }

  firmar(firma:Firma) { 
    return this.http.post<any>('http://localhost:8083/api/firmas/firmar-documento', firma)
      .toPromise()
      .then(res => res.data)
  }

}
