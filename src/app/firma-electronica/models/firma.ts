export interface Firma{
    idDocumento?: number;
    cerFile?: number;
    pfxFile?: string;
    keyFile?: string;
    documentoBase64?: string;
    cerBase64?: string;
    certificadoBase64?: string;
    keyBase64?: string;
    contrasena?: string;
}