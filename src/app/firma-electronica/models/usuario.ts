export interface Usuario {
    id?: number;
    numExpediente?: string;
    nombre?: string;
    apellidop?: string;
    apellidom?: string;
    email?: string;
    password?: string;
    codigoRol?: string;
    estatus?: string;
}