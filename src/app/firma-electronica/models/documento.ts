export interface Documento{
    id?: number;
    tipoDocumento?: string;
    tipoArchivo?: string;
    nombreDocumento?: string;
    descripcion?: string;
    rutaDocumento?: string;
    contenido?: string;
    documentoBase64?: any;
    documentoFirmadoBase64?: any;
}