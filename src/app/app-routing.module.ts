import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { AppLayoutComponent } from './layout/app.layout.component';

const routerOptions: ExtraOptions = {
    anchorScrolling: 'enabled'
};

const routes: Routes = [
    { path: '',  loadChildren: () => import('./firma-electronica/components/login/login.module').then(m => m.LoginModule) },
    {
        path: 'admin', 
        component: AppLayoutComponent,
        children: [
            { path: '',  data: { breadcrumb: 'Inicio' }, loadChildren: () => import('./firma-electronica/components/inicio/inicio.module').then(m => m.InicioModule) },
            { path: 'mis-documentos', data: { breadcrumb: 'Mis Documentos' }, loadChildren: () => import('./firma-electronica/components/documentos/documentos.module').then(m => m.DocumentosModule) },
            { path: 'usuarios', data: { breadcrumb: 'Usuarios' }, loadChildren: () => import('./firma-electronica/components/usuarios/usuarios.module').then(m => m.UsuariosModule) },
            { path: 'configuracion', data: { breadcrumb: 'Configuración' }, loadChildren: () => import('./firma-electronica/components/configuracion/configuracion.module').then(m => m.ConfiguracionModule) },
   
        ]
    },
    { path: '**', redirectTo: '/notfound' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, routerOptions)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
